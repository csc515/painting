package edu.missouristate.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import edu.missouristate.domain.Painting;
import edu.missouristate.service.PaintingService;

// @RestController - returns JSON or XML or TEXT or ..... 
// @Controller - returns a page, unless you add the @ResponseBody annotation above it

@Controller
public class PaintingController {

	@Autowired
	PaintingService paintingService;
	
	@GetMapping(value="/")
	public String getIndex(Model model) {
		List<Painting> paintingList = paintingService.getAllPaintings();
		model.addAttribute("paintingList", paintingList);
		return "index";
	}
	
	@GetMapping(value="/addPainting")
	public String getAddPainting(Model model) {
		return "addPainting";
	}
	
	@PostMapping(value="/addPainting")
	public String postPainting(Model model, @RequestBody Painting painting) {
		// TODO - call the service and save the painting to the database
		return "redirect:/";
	}
	
	@ResponseBody
	@GetMapping(value="/hello")
	public String getHello() {
		return "hello world!";
	}
	
	@GetMapping(value="/sw.js")
	public String getSw(Model model) {
		return "";
	}
}
