package edu.missouristate.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "paintings")
public class Painting {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id; // `id` INT NOT NULL PRIMARY KEY AUTO_INCREMENT,

	@Column
	private String url; // `url` VARCHAR(2048) NOT NULL,

	@Column
	private String paintingName; // `painting_name` VARCHAR(256) NOT NULL,

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getPaintingName() {
		return paintingName;
	}

	public void setPaintingName(String paintingName) {
		this.paintingName = paintingName;
	}

}
