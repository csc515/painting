package edu.missouristate.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.missouristate.domain.Painting;
import edu.missouristate.repository.PaintingRepository;

@Service("paintingService")
public class PaintingService {

	@Autowired
	PaintingRepository paintingRepo;

	@Transactional
	public void addPainting(Painting painting) {
		// TODO Auto-generated method stub
		// add painting using CRUD library!
		paintingRepo.save(painting);
	}
	
	@Transactional
	public void editPainting(Painting painting) {
		// add painting using CRUD library!
		paintingRepo.save(painting);
	}
	
	@Transactional
	public void deletePainting(Painting painting) {
		paintingRepo.delete(painting);
	}

	public List<Painting> getAllPaintings() {
		return (List<Painting>) paintingRepo.findAll(); // select * from paintings; <==hibernate
	}
	
}
