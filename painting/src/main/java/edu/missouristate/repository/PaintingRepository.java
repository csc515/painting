package edu.missouristate.repository;

import org.springframework.data.repository.CrudRepository;

import edu.missouristate.domain.Painting;

public interface PaintingRepository extends CrudRepository<Painting, Integer>, PaintingRepositoryCustom {
	// Spring Data abstract methods go here
	
	
}
