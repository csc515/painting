package edu.missouristate.repository;

import java.util.List;

import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import edu.missouristate.domain.Painting;
import edu.missouristate.domain.QPainting;

@Repository
public class PaintingRepositoryImpl extends QuerydslRepositorySupport implements PaintingRepositoryCustom {
	
	QPainting paintingTable = QPainting.painting;
	
	public PaintingRepositoryImpl() {
		super(Painting.class);
	}

	// Object Oriented SQL goes here...
	public Painting getPaintingByName(String name) {
		return (Painting) from(paintingTable)
				.where(paintingTable.paintingName.eq(name))
				.orderBy(paintingTable.id.asc())
				.limit(1)
				.fetch();
	}
	
	public List<Painting> getBootListByName(String name) {
		return from(paintingTable)
				.where(paintingTable.paintingName.eq(name))
				.orderBy(paintingTable.paintingName.asc())
				.fetch();
	}
	
}
