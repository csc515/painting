<%@ include file="/WEB-INF/views/includes/include.jsp" %>
<!doctype html>
<html lang="en">
<head>
	<title>Paintings</title>
	<!-- Bootstrap 4.5 CSS-->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">	
	<!-- Bootstrap JS Requirements -->
	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h1>Painting Manager</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<a href="<%=request.getContextPath() %>/addPainting" class="btn btn-primary" >Add Painting</a>
				<table class="table table-hover table-striped table-bordered mt-2">
					<tr>
						<th>ID</th>
						<th>URL</th>
						<th>NAME</th>
						<!-- add other things if you like -->
					</tr>
					<!-- 
						* Insert Data into your table (using add button) and then
						* Build a table here and loop through the data and display it	
					-->
					<!-- Handle when no data found -->
					<c:if test="${empty paintingList}">
						<tr><td colspan="3">No Records Found</td></tr>
					</c:if>
					<!-- Loop Here -->
					<c:if test="${not empty paintingList}">
						<!-- 
							 Add <tr> elements and <td> elements below inside 
							 of the c:forEach tags (build the body of the table)
						-->
						<c:forEach var="painting" items="${paintingList}" > 
							<tr><td colspan="3">${painting.id}</td></tr>
							<!-- 
								${painting.url},${painting.paintingName} ....
							 -->
							 <!-- 
							 	<tr>
							 		<td>${painting.id}</td>
							 		<td>
							 			<a target="_blank" href="https://someurl.com/somepath/imagename.png">
							 				${painting.url}
							 			</a>	
							 		</td>
							 		<td>${painting.name}</td>							 		
							 	</tr>
							  -->
						</c:forEach>
					</c:if>
				</table>
			
			</div>
		</div>
	</div>
</body>
</html>
